# Money manager

Application that helps me monitoring my hope budget.

## Required functionalities

1. Units
  - record 
    * symbol: string, Pk
    * name: string
  - Add units
  - Modify units
  - Remove units

2. Expense category
  - record
    * symbol: string, Pk
    * description: string
  - Add category
  - Modify category
  - Remove category

3. Expense
  - record:
    * id: int, Pk
    * categorySymbol: Fk
    * unitSymbol: Fk
    * date: Date
    * amount: DECIMAL(60, 2)
    * description: string
  - add
  - modify
  - remove

4. Create resource
  - record [ resourceId, unitId, name ]
  - add
  - modify
  - remove

5. Resource history
  - record [ stateId, resourceId, date, amount ]
  - add
  - modify
  - remove
  - interpolation: last point

6. Expense summary
  - monthly breakdown
  - year total

## User interface specification

- `money` : entry point command
- `money unit add <symbol> <name>`
  - verify uniqueness of symbol
  - return ok (0, print OK), or error code (1, print error: info)
- `money unit del <symbol>`
  - return ok (0, print OK), or error code (1, print error: info)
  - ask if any other table has records using that symbol
- `money unit`
  - print table of units
